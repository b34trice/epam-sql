/****** Script for SelectTopNRows command from SSMS  ******/

/*1*/
SELECT [OrderID]
      ,[ShippedDate]
      ,[ShipVia]
FROM [Northwind].[dbo].[Orders] WHERE (ShippedDate>='1998-05-06' AND ShipVia>=2)

/*2*/
SELECT OrderID,
	CASE WHEN (ShippedDate IS NULL) THEN 'Not Shipped'
	END as ShippedDate
FROM Northwind.dbo.Orders WHERE ShippedDate IS NULL

/*3*/
SELECT OrderID as 'Order Id',
	CASE WHEN (ShippedDate IS NULL) THEN 'Not Shipped'
	END AS 'Shipped Date'
FROM [Northwind].[dbo].[Orders] WHERE (ShippedDate>'1998-05-06' OR ShippedDate IS NULL)