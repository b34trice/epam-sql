/*1*/
SELECT ContactName,Country FROM Customers WHERE Country IN ('USA','Canada') ORDER BY ContactName,Country

/*2*/
SELECT ContactName,Country FROM Customers WHERE Country NOT IN ('USA','Canada') ORDER BY ContactName

/*3*/
SELECT DISTINCT Country FROM Customers