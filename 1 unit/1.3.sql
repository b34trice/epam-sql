/*1*/
Select Distinct OrderID from [Order Details] WHERE Quantity BETWEEN 3 AND 10;

/*2*/
Select CustomerID,Country from Customers WHERE Country BETWEEN 'b' AND 'h' ORDER BY Country

/*3*/
Select * from Customers WHERE (Country >= 'b' AND Country <= 'h')