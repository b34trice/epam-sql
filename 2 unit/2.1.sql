/*1*/
Select SUM((UnitPrice-Discount)*Quantity) as 'Totals' from [Order Details]

/*2*/
Select Count(*) - COUNT(ShippedDate) as 'Count' from Orders

/*3*/
Select (Count(distinct CustomerID)) as 'Count' from Orders