/*1*/
Select Year(OrderDate) as 'Year', Count(OrderID) as 'Count' from Orders GROUP BY Year(OrderDate)
Select Count(OrderID) from Orders

/*2*/
Select (FirstName+' '+LastName) as 'Seller',
Count(OrderID) as 'Amount'
from Orders as orderItem inner join Employees as employee on orderItem.EmployeeID = employee.EmployeeID
group by orderItem.EmployeeID,employee.FirstName,employee.LastName order by Amount desc

/*3*/
Select COUNT(OrderID) as 'Count' from Orders
inner join Customers on Orders.CustomerID = Customers.CustomerID
inner join Employees on Orders.EmployeeID = Employees.EmployeeID
Where (YEAR(OrderDate)=1998)

/*4*/
Select * from Customers as customer, Employees as employee
where (customer.City = employee.City) 

/*5*/
Select distinct
customer1.CustomerID as 'Id1',
customer2.CustomerID as 'Id2',
customer1.City as 'City1',
customer2.City as 'City2'
from Customers as customer1 join Customers as customer2
on customer1.City = customer2.City where(customer1.CustomerID <> customer2.CustomerID)

/*6*/
select employee.LastName AS 'Employee', boss.LastName AS 'Boss'
FROM Employees AS employee JOIN Employees AS boss
ON employee.ReportsTo = boss.EmployeeID