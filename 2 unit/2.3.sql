/*1*/
SELECT empployee.EmployeeID, territorie.TerritoryDescription
FROM 
	EmployeeTerritories AS employeTerritorie 
	JOIN Territories AS territorie	ON employeTerritorie.TerritoryID = territorie.TerritoryID 
	JOIN Region AS region ON territorie.RegionID = region.RegionID
	JOIN Employees AS empployee	ON empployee.EmployeeID = employeTerritorie.EmployeeID
WHERE region.RegionDescription = 'Western'

/*2*/
SELECT customer.ContactName, COUNT(orderItem.OrderID) AS 'Result'
FROM Customers AS customer LEFT JOIN Orders AS orderItem
ON customer.CustomerID = orderItem.CustomerID
GROUP BY customer.CustomerID, customer.ContactName ORDER BY Result