/*1*/
SELECT CompanyName FROM Suppliers
WHERE SupplierID IN(SELECT SupplierID FROM Products	WHERE UnitsInStock = 0)

/*2*/
SELECT employee.EmployeeID
FROM Employees AS employee
WHERE 150 < (SELECT COUNT(orderItem.OrderID) FROM Orders AS orderItem WHERE orderItem.EmployeeID = employee.EmployeeID)

/*3*/
SELECT *
FROM Customers AS customer
WHERE NOT EXISTS(SELECT * FROM Orders AS orderItem WHERE orderItem.CustomerID = customer.CustomerID)